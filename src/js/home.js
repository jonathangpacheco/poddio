(function () {
    // $("#slider").noUiSlider({
    //     start: [ 0, 600 ],
    //     range: {
    //         'min': 0,
    //         'max': 600
    //     },
    //     connect: true,
    //     // Set some default formatting options.
    //     // These options will be applied to any Link
    //     // that doesn't overwrite these values.
    //     format: wNumb({
    //         decimals: 1
    //     })
    // });

    // Any selector is acceptable, so we'll
    // select both inputs.
    function toVal(value) {
        var str = window.Utils.convertToString(value);
        $(this).val(str);
    }
    function buildSlider( exists ) {
        var config = window.Qast.config;
        var $audio = $("<audio>");
        $('#slider').attr('disabled', 'disabled');

        $audio.prop("controls", false);
        $audio.attr("preload", "auto");
        $audio.on("durationchange", function () {
            $('#slider').removeAttr('disabled');
            if ( exists == null ) {
                $("#start, #end").on("change", function() {
                    $("#slider").val( [window.Utils.convertToSeconds($("#start").val()), window.Utils.convertToSeconds($("#end").val())] );
                });
            }
            $("#slider").noUiSlider({
                start: [window.Utils.convertToSeconds($("#start").val()), window.Utils.convertToSeconds($("#end").val())],
                range: {
                    'min': 0,
                    'max': $audio[0].duration
                },
                limit: 600,
                behaviour: "tap-drag",
                connect: true,
                // Set some default formatting options.
                // These options will be applied to any Link
                // that doesn't overwrite these values.
                format: wNumb({
                    decimals: 1
                })
            }, true);
            $("#slider").Link('lower').to($("#start"), toVal);
            $("#slider").Link('upper').to($("#end"), toVal);

            $audio.attr("src", "");
        });
        $audio.attr("src", config.enclosure );
    }


    $(window).on("qast/form/update", function () {
        buildSlider( true );
    });
    buildSlider();

}());

// Cut the mustard

(function( doc ) {
    "use strict";

    var Embed = function Embed() {
            
            this.init();

            return this;
        };

    Embed.prototype = {
        "config": {
            "elemSelector": ".qast-media"
        },
        "init": function init() {
            this.initVars();

            if ( this.$elems ) {
                this.renderFrames();
            }
        },
        "initVars": function initVars() {
            this.$elems = this.getElems();
        },
        "getElems": function getElems() {
            return doc.querySelectorAll( this.config.elemSelector );
        },
        "renderFrames": function renderFrames() {
            [].forEach.call( this.$elems, this.buildFrame.bind(this) );
        },
        "buildFrame": function buildFrame( v ) {
            var iframe = doc.createElement( "iframe" ),
                embedURL = v.getAttribute("data-qast-embed");

            iframe = this.styleFrame( iframe );
            iframe.src = embedURL;

            this.placeFrame(v, iframe);
        },
        "styleFrame": function styleFrame( iframe ) {
            iframe.width = "100%";
            iframe.frameborder = "0";
            iframe.style.display = "block";
            iframe.style.padding = "0";
            iframe.style.border = "none";
            iframe.style.height = "80px";
            iframe.style.maxWidth = "600px";
            iframe.style.marginRight = "auto";
            iframe.style.marginLeft = "auto";
            iframe.style.boxShadow = "0 1px 3px rgba(0,0,0,0.15)";

            return iframe;
        },
        "placeFrame": function placeFrame( elem, iframe ) {
            elem.parentNode.replaceChild( iframe, elem );
        }
    };

    new Embed();
}( document ));
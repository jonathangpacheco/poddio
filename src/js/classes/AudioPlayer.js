(function() {
    "use strict";

    var AudioPlayer = function AudioPlayer( elem ) {
            this.UI = new UI( elem );
            this.config = window.Qast.config;
            this.feed = this.config.feed;
            this.episode = this.config.url;

            if ( this.config.enclosure ) {
                this.initVars();
                this.bindEvents();
            }

            this.init();

            return this;
        },
        mouseEnterEventListener = {};

    AudioPlayer.prototype = {
        "defaults": {
            "playButton": ".player__play",
            "backwardButton": ".player__backward",
            "forwardButton": ".player__forward",
            "restartButton": ".restart",
            "playheadTime": ".player__time",
            "messaging": ".messaging",
            "playheadTimeline": ".player__timeline",
            "forwardIncrement": 30,
            "backwardIncrement": 10,
            "detailsUrl": location.protocol + '//' + location.hostname + (location.port ? ':' + location.port : '') + "/api/podcast/details"
        },
        "initVars": function initVars() {
            this.src = this.config.enclosure;
            this.start = this.config.start;
            this.end = this.config.end;
            this.currentTime = 0;
            this.prevCurrentTime = 0;
            this.length = this.end;
            this.canPlay = false;
            this.wantsToPlay = false;
            this.isPlaying = false;
            this.isPaused = false;
            this.buildMouseEnterEventListener();
        },
        "bindEvents": function bindEvents() {
            this.UI.$player.addEventListener("durationchange", this.getStartEnd.bind(this), false);
            this.UI.$player.addEventListener("timeupdate", this.updatePosition.bind(this), false);
            this.UI.buttons.$play.addEventListener("mouseenter", mouseEnterEventListener, false );
            this.UI.$player.addEventListener("playing", function() {
                Utils.info("Audio Playing");
                this.UI.playing();
                this.isPlaying = true;
                this.isPaused = false;
            }.bind(this) );

            this.UI.$player.addEventListener("waiting", function() {
                Utils.info("Player Waiting");
                if ( this.wantsToPlay ) {
                    this.UI.loading.bind(this.UI);
                }
            }.bind(this), false );
            this.UI.$player.addEventListener("ended", this.updatePosition.bind(this), false );
            this.UI.buttons.$play.addEventListener("click", this.onPlayPress.bind(this), false );
            this.UI.buttons.$restart.addEventListener("click", this.onRestartPress.bind(this), false );
            this.UI.playhead.$seeker.addEventListener("click", function(e) {
                fastdom.read(function() {
                    var relativeLeft = e.clientX - this.UI.buttons.$play.offsetWidth - 10;

                    this.seekTime( relativeLeft );
                }.bind(this));
            }.bind(this), false);
            this.UI.$player.addEventListener("canplay", function () {

                // If we are ready and we have not been actively played
                // or paused, set the current time to the start point
                if ( this.UI.$player.readyState > 0 && !this.isPlaying && !this.isPaused ) {
                    this.UI.$player.currentTime = this.start;
                }

                // If we are ready to play but we are outside the bounds,
                // set the current time to the start
                if ( (this.isBeforeStart() || this.isAfterEnd()) && this.readyState > 0 ) {
                    this.UI.$player.currentTime = this.start;
                }
            }.bind(this));
            this.UI.$player.addEventListener("canplaythrough", function() {
                this.canPlay = true;
                this.UI.canPlay();

                if ( this.wantsToPlay ) {
                    Utils.info("Can Play Through & Wants to Play");
                    this.UI.$player.play();
                }
            }.bind(this) );
            this.UI.$player.addEventListener("pause", function () {
                this.isPlaying = false;
                this.isPaused = true;
                this.UI.paused();
            }.bind(this));
            this.UI.$player.addEventListener("suspend", function () {
                Utils.info("Player Suspended");
                if ( !this.isPaused && !this.isPlaying && this.wantsToPlay ) {
                    this.UI.$player.play();
                }
            }.bind(this));
        },
        "getStartEnd": function getStartEnd() {
            this.length = this.end - this.start;

            // If we "start" after the file actually ends
            if ( this.UI.$player.duration < this.start ) {
                // If we "end" after the file actually ends
                if ( this.UI.$player.duration < this.end ) {

                    // We "end" at the end of the file
                    this.end = this.UI.$player.duration;
                }
                // Our new "start" is relative to our "end"
                this.start = this.end - this.length;

            // If we "end" after the file actually ends
            } else if ( this.UI.$player.duration < this.end ) {
                // Bump up the "end" to the end of the file
                this.end = this.UI.$player.duration;
            }

            // Re-calculate the length after our adjustments
            this.length = this.end - this.start;

            // Update the UI with our new length
            this.UI.updatePlayhead( this.length );
        },
        "isBeforeStart": function isBeforeStart() {
            return this.UI.$player.currentTime < this.start;
        },
        "isAfterEnd": function isAfterEnd() {
            return this.UI.$player.currentTime > this.end;
        },
        "isAtEnd": function isAtEnd() {
            return this.UI.$player.currentTime === this.end;
        },
        "isAtOrAfterEnd": function isAtOrAfterEnd() {
            return this.UI.$player.currentTime >= this.end;
        },
        "updatePosition": function updatePosition() {
            var position;

            // If the current time as after our calculated end
            // and we are actively playing, pause.
            if ( this.isAfterEnd() && this.isPlaying ) {
                this.wantsToPlay = false;
                this.pause();
            }

            // If the current time is before our start point, or
            // if our current time is after our calculated end and
            // we have not been told we are able to play,
            // move back to the start
            if ( (this.isBeforeStart() || this.isAfterEnd()) && !this.canPlay ) {
                this.UI.$player.currentTime = this.start;
            }

            // Set our "previous" current time for historical purposes
            this.prevCurrentTime = this.currentTime;

            // Our current time local var is relative to our start point
            this.currentTime = this.UI.$player.currentTime - this.start;

            // Our position is how much length we have left
            position = this.length - this.currentTime;


            // If we don't have anytime left
            if ( position <= 0 ) {

                // Update the UI
                this.UI.updatePlayhead( 0 );
                this.updateTimeline( 0 );

                // If this is because we've reached the end,
                // pause things
                if ( position === 0 && this.prevCurrentTime === this.end ) {
                    this.pause();
                    this.UI.paused();
                }

            // If we have time left
            } else {

                // Update the UI with the time left
                this.UI.updatePlayhead( this.length - this.currentTime );
                this.updateTimeline( (this.currentTime / this.length) * 100 );
            }
        },
        "seekTime": function seekTime( pos ) {
            var seekerWidth = this.UI.playhead.$seeker.offsetWidth,
                perc = pos / seekerWidth;

            this.wantsToPlay = true;

            this.UI.$player.currentTime = this.start + (this.length * perc);
        },
        "init": function init() {
            if ( (!this.config.enclosure || !this.config.image) && this.config.feed ) {
                this.getPodcastDetails();
            } else if ( !this.config.feed && !this.config.enclosure ) {
                this.fileError("No audio file found.");
            }
        },
        "onDetailsRequestSuccess": function onDetailsRequestSuccess() {

            if ( this.request.responseText !== "" ) {

                // Success!
                var resp = JSON.parse(this.request.responseText);

                this.config = Utils.extend( this.config, resp );

                if ( resp.image ) {
                    this.UI.$image.src = this.config.image;
                } else {
                    this.placeFallbackImage();
                }

                if ( !this.config.enclosure ) {
                    if ( resp.enclosure ) {
                        this.loadPlayer();
                    } else {
                        this.fileError("No audio file found.");
                    }
                }
            } else {
                this.placeFallbackImage();

            }
        },
        "loadPlayer": function loadPlayer() {
            this.UI.$player.src = this.config.enclosure;
            this.UI.canPlay();

            this.initVars();
            this.bindEvents();
        },
        "onDetailsRequestFail": function onDetailsRequestFail( err ) {
            this.fileError( err );

            this.placeFallbackImage();
        },
        "placeFallbackImage": function placeFallbackImage() {
            if ( this.UI.$image.getAttribute("data-og") !== "" ) {
                this.UI.$image.src = this.UI.$image.getAttribute("data-og");
            }
        },
        "getPodcastDetails": function getPodcastDetails() {
            this.request = new XMLHttpRequest();
            this.request.open('GET', this.buildAjax(), true);

            if ( this.request.overrideMimeType ) {
                this.request.overrideMimeType("text/plain; charset=x-user-defined");
            }

            this.request.onload = function() {
                if (this.request.status >= 200 && this.request.status < 400) {
                    this.onDetailsRequestSuccess();
                } else {
                    this.onDetailsRequestFail("We reached our target server, but it returned an error.");
                }
            }.bind(this);

            this.request.onerror = function() {
                this.onDetailsRequestFail("Failure to get Podcast Details.");
            }.bind(this);

            this.request.send();

        },
        "buildAjax": function buildAjax() {
            // console.log("Building AJAX URL.");

            var url = this.defaults.detailsUrl;

            if ( this.feed != null && this.feed !== "undefined" ) {
                // console.info("Podcast Feed: " + this.feed);
                url += "?url=";
                url += this.feed;
            }
            url += "&episode=";
            url += this.episode;
            if ( this.src ) {
                url += "&file=";
                url += this.src.indexOf("#t=") > -1 ? this.src.substring(0, this.src.indexOf("#t=")) : this.src;
            }
            if ( this.config.soundcloud.objectUrl ) {
                url += "&sc=";
                url += this.config.soundcloud.objectUrl;
            }

            // console.debug("Detail Request URL: " + url);

            return url;
        },
        "buildMouseEnterEventListener": function buildMouseEnterEventListener() {
            mouseEnterEventListener.instance = this;
            mouseEnterEventListener.handleEvent = function handleEvent( ev ) {
                // this.instance.beginLoad();
                ev.target.removeEventListener("mouseenter", mouseEnterEventListener, false);
            };
        },
        "beginLoad": function beginLoad() {
            Utils.trace("Begin Load");
            this.UI.$player.load();
            this.UI.loading();
        },
        "onPlayPress": function onPlayPress() {

            // On play press,
            // if we are not actually playing
            if ( !this.isPlaying ) {
                // If we have not yet been told we can play,
                // start loading the file
                if ( !this.canPlay ) {
                    this.beginLoad();

                // If we are able to play
                } else {

                    // If we have an endpoint,
                    // we have to check our position against it.
                    if ( this.end ) {

                        // If we are past the end but we want to
                        // play, restart. Otherwise, play
                        if ( this.isAfterEnd() ) {
                            this.restart();
                        } else {
                            this.UI.$player.play();
                        }

                    // If we have no endpoint, just play
                    } else {
                        this.UI.$player.play();
                    }
                }

                // Register as wanting to play
                this.wantsToPlay = true;

            // If we are already playing,
            // pause
            } else {
                this.UI.$player.pause();

                // ???
                this.wantsToPlay = true;
            }
        },
        "play": function play() {
            // console.log("Our Play was called");
            this.UI.$player.play();
            this.UI.playing();
        },
        "pause": function pause() {
            // console.trace("Our Pause was called");
            this.UI.$player.pause();
            this.UI.paused();
        },
        "restart": function restart() {
            this.onRestartPress();
            if ( this.isPlaying ) {
                this.play();
            } else {
                this.wantsToPlay = false;
            }
        },
        "onRestartPress": function onRestartPress() {
            this.UI.$player.currentTime = this.start;
        },
        "updateTimeline": function updateTimeline( length ) {
            this.UI.updateTimeline( length );
        },
        "fileError": function fileError( err ) {
            this.UI.error( err, this.config.url );
            this.UI.disabled();
            this.UI.buttons.$play.removeEventListener("click", this.onPlayPress.bind(this), false );
            this.UI.buttons.$restart.removeEventListener("click", this.onRestartPress.bind(this), false );
        }
    };

    window.AudioPlayer = AudioPlayer;
}());
(function() {
    "use strict";

    var UI = function UI( elem ) {
            this.$elem = elem;
            this.$player = this.$elem.querySelector("audio");
            this.$image = this.$elem.querySelector(".player__image img");
            this.$messaging = this.$elem.querySelector( this.defaults.messaging );
            this.playhead = {};
            this.playhead.$time = this.$elem.querySelector( this.defaults.playheadTime );
            this.playhead.$timeline = this.$elem.querySelector( this.defaults.playheadTimeline );
            this.playhead.$seeker = document.getElementById( this.defaults.seeker );
            this.buttons = {};
            this.buttons.$play = this.$elem.querySelector( this.defaults.playButton );
            this.buttons.$playLabel = this.buttons.$play.querySelector("b");
            this.buttons.$restart = this.$elem.querySelector( this.defaults.restartButton );

            this.$player.controls = false;

            if ( this.$image.src !== "" ) {
                setTimeout(function() {
                    this.$elem.classList.add("has-image");
                }.bind(this), 250);
            } else {
                this.$image.onload = function () {
                    setTimeout(function() {
                        this.$elem.classList.add("has-image");
                    }.bind(this), 250);
                }.bind(this);
            }

            return this;
        };

    UI.prototype = {
        "defaults": {
            "playButton": ".player__play",
            "backwardButton": ".player__backward",
            "forwardButton": ".player__forward",
            "restartButton": ".restart",
            "playheadTime": ".player__time",
            "messaging": ".messaging",
            "playheadTimeline": ".player__timeline",
            "seeker": "player__controls",
            "forwardIncrement": 30,
            "backwardIncrement": 10,
            "detailsUrl": "/api/podcast/details"
        },
        "playing": function playing() {

            this.buttons.$play.classList.add("paused");
            this.buttons.$play.setAttribute("title", "Pause");
            this.buttons.$playLabel.innerText = "Pause";
        },
        "canPlay": function canPlay() {

            this.buttons.$play.classList.remove("loading");
            this.buttons.$play.disabled = false;
            this.buttons.$playLabel.innerText = "Play";
            this.buttons.$restart.classList.remove("disabled");
        },
        "paused": function paused() {
            this.buttons.$play.classList.remove("paused");
            this.buttons.$play.setAttribute("title", "Play");
            this.buttons.$playLabel.innerText = "Play";
        },
        "loading": function loading() {
            Utils.trace("UI Loading");
            this.buttons.$play.classList.remove("paused");
            this.buttons.$play.classList.add("loading");
            this.buttons.$play.disabled = true;
            this.buttons.$play.setAttribute("title", "Loading...");
            this.buttons.$playLabel.innerText = "Loading...";
        },
        "error": function error( message, url ) {
            this.buttons.$play.classList.add("not-found");
            this.buttons.$play.setAttribute("title", message);
            this.buttons.$playLabel.innerText = message;
            this.$messaging.classList.add("error");
            this.$messaging.innerHTML = message + " <a href='mailto:bugs@qast.fm?subject=" + message + " (" + url + ")&body=Qast is still pretty cool!'>Report</a>";
        },
        "disabled": function disabled() {
            this.buttons.$play.disabled = true;
            this.buttons.$restart.classList.add("disabled");
            this.playhead.$time.classList.add("disabled");
            this.buttons.$play.classList.remove("loading");

        },
        "updatePlayhead": function updatePlayhead( length ) {
            var timestamp = length || 0;

            this.playhead.$time.innerHTML = Utils.convertToString( timestamp );
        },
        "updateTimeline": function updateTimeline( width ) {
            this.playhead.$timeline.style.width = width + "%";
        }
    };

    window.UI = UI;
}());
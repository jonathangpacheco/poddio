/*requires: jQuery, Poddio.List*/
var Poddio = Poddio || {};
Poddio.Data = (function () {

    /**
    * Variables
    */
    var $body = $("body");

    /**
    * Constructor
    */
    function Data() {
        this.NS = "PoddioData:";
        this.values = [];
    }

    /**
    * Instance methods and properties
    */

    // Trigger
    Data.prototype.trigger = function (ev, data) {
        $body.trigger(this.NS + ev, data);
    };

    // Load url
    Data.prototype.loadUrl = function (url, opts) {
        var
            self = this,
            defaults = {
                dataType: "json",
                type: "GET",
                url: "/api/feed"
            },
            config = $.extend(defaults, {
                data: {
                    url: url
                }
            }, opts),
            def = $.ajax(config);

        // On success
        def.done(function (json) {
            console.log("success:", arguments);
            self.values = json;
            self.trigger("load", self);
        });

        // On error
        def.fail(function () {
            console.log("fail:", arguments);
            self.trigger("error", [this].concat(arguments));
        });
        return def;
    };

    /**
    * Expose
    */
    return Data;
}());

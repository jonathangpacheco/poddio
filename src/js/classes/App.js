/*requires: jQuery, Poddio.Data, Poddio.List*/
var Poddio = Poddio || {};
Poddio.App = (function () {

    /**
    * Variables
    */
    var $body = $("body");

    /**
    * Constructor
    */
    function App() {
        this.initVars();
        this.setBinds();
        this.addElems();
    }

    /**
    * Instance methods and properties
    */

    // Init vars
    App.prototype.initVars = function () {
        this.data = new Poddio.Data();
        this.list = new Poddio.List();
    };

    // Set binds
    App.prototype.setBinds = function () {
        var self = this;

        // Update podcast list on data load
        $body.on(this.data.NS + "load", function () {
            self.list.update(self.data.values);
        });
    };

    // Add elements
    App.prototype.addElems = function () {
        $body.append(this.list.$el);
    };

    /**
    * Expose
    */
    return App;
}());

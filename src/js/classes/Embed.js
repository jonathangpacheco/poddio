(function() {
    "use strict";

    var Embed = function Embed( elem ) {
            this.$elem = elem;

            this.init();

            return this;
        };

    Embed.template = typeof HTMLTemplateElement !== "undefined";
    Embed.prototype = {
        "defaults": {
            "api": "/embed.json"
        },
        "init": function init( elem ) {
            this.initVars();
            this.bindEvents();
        },
        "initVars": function initVars() {
            this.$url = this.$elem.querySelector(".url");
            this.$submit = document.getElementById("submit");
            this.$preview = document.getElementById("embed-preview");
            this.$code = document.getElementById("embed-code");
            this.$twitter = document.getElementById("twitter-code");
            this.$feeds = document.getElementById("feeds");
            this.$sources = document.getElementById("sources");
            this.$start = document.getElementById("start");
            this.$end = document.getElementById("end");
            this.hasFeeds = this.$feeds.children.length ? true : false;
            this.hasSources = false;
            if ( Embed.template ) {
                this.template = {
                    "feeds": new Temple( document.getElementById("template-feeds")),
                    "no-feeds": new Temple( document.getElementById("template-no-feeds")),
                    "sources": new Temple( document.getElementById("template-sources"))
                };
            }
        },
        "bindEvents": function bindEvents() {
            this.$elem.addEventListener("submit", this.validateForm.bind(this));
            this.$url.addEventListener("change", this.clearForm.bind(this));
            this.$code.addEventListener("focus", function () {
                this.$code.select();
            }.bind(this));
            this.$feeds.addEventListener("click", function (ev) {
                if ( ev.target.hasAttribute("data-reveal") ) {
                    if ( ev.target.classList.contains("is-expanded") ) {
                        ev.target.classList.remove("is-expanded");
                        ev.target.classList.add("was-expanded");
                        ev.target.classList.add("icon--plus");
                        ev.target.classList.remove("icon--minus");
                        document.getElementById( ev.target.getAttribute("data-reveal") ).classList.remove("is-expanded");
                        document.getElementById( ev.target.getAttribute("data-reveal") ).classList.add("was-expanded");
                    } else {
                        ev.target.classList.add("is-expanded");
                        ev.target.classList.remove("icon--plus");
                        ev.target.classList.add("icon--minus");
                        document.getElementById( ev.target.getAttribute("data-reveal") ).classList.add("is-expanded");
                    }
                    ev.preventDefault();
                    ev.stopPropagation();
                }
            });
            this.$sources.addEventListener("click", function (ev) {
                if ( ev.target.hasAttribute("data-reveal") ) {
                    if ( ev.target.classList.contains("is-expanded") ) {
                        ev.target.classList.remove("is-expanded");
                        ev.target.classList.add("was-expanded");
                        ev.target.classList.add("icon--plus");
                        ev.target.classList.remove("icon--minus");
                        document.getElementById( ev.target.getAttribute("data-reveal") ).classList.remove("is-expanded");
                        document.getElementById( ev.target.getAttribute("data-reveal") ).classList.add("was-expanded");
                    } else {
                        ev.target.classList.add("is-expanded");
                        ev.target.classList.remove("icon--plus");
                        ev.target.classList.add("icon--minus");
                        document.getElementById( ev.target.getAttribute("data-reveal") ).classList.add("is-expanded");
                    }
                    ev.preventDefault();
                    ev.stopPropagation();
                }
            });
        },
        "validateForm": function validateForm(ev) {
            this.disableForm();
            this.collapseExtras( this.$sources );
            this.collapseExtras( this.$feeds );
            if ( this.$url.value !== "" ) {
                if ( Embed.template ) {
                    this.getEmbedJson();
                    if ( ev ) {
                        ev.preventDefault();
                    }
                }
            } else {
                if ( ev ) {
                    ev.preventDefault();
                }
            }
        },
        "disableForm": function disableForm() {
            this.$submit.disabled = true;
            this.$submit.innerText = "Generating...";
            this.$preview.classList.add("loading");
            this.$code.classList.add("loading");
            this.$twitter.classList.add("loading");
        },
        "enableForm": function enableForm() {
            this.$submit.disabled = false;
            this.$submit.innerText = "Generate My Snippet!";
            this.$preview.classList.remove("loading");
            this.$code.classList.remove("loading");
            this.$twitter.classList.remove("loading");
        },
        "clearForm": function clearForm() {
            this.$start.value = "00:00";
            this.$end.value = "01:00";
            this.$feeds.innerHTML = "";
            this.hasFeeds = false;
            this.$sources.innerHTML = "";
            this.hasSources = false;
            this.validateForm( false );
        },
        "collapseExtras": function collapseExtras( $extras ) {
            var $trigger = $extras.querySelector("[data-reveal]") || false,
                $target;

            if ( $trigger ) {
                $target = document.getElementById( $trigger.getAttribute("data-reveal") );

                $trigger.classList.remove("is-expanded");
                $trigger.classList.remove("icon--minus");
                $trigger.classList.add("was-expanded");
                $trigger.classList.add("icon--plus");
                $target.classList.remove("is-expanded");
                $target.classList.add("was-expanded");
            }
        },
        "getEmbedJson": function getEmbedJson() {
            var self = this;

            this.request = new XMLHttpRequest();
            this.request.open('GET', this.defaults.api + this.serializeForm(), true);
            this.request.overrideMimeType("text/plain; charset=x-user-defined");

            this.request.onreadystatechange = function() {
                if (this.readyState === 4) {
                    if (this.status >= 200 && this.status < 400) {
                        self.requestSuccess.call( self, this.responseText );
                    } else {
                        // console.log("error");
                    }
                    self.enableForm.call( self );
                }
            };

            this.request.onerror = function() {
                // console.log("error");
                this.enableForm();
            }.bind(this);

            this.request.send();
            this.request = null;
        },
        "serializeForm": function serializeForm() {
            var queries = "";

            [].forEach.call( this.$elem.querySelectorAll("input, select, textarea"), function(v, i) {

                if ( v.value === "" || (v.type === "radio" && !v.checked ) ) {
                    return;
                }
                if ( i === 0 ) {
                    queries += "?";
                } else {
                    queries += "&";
                }

                queries += v.name;
                queries += "=";
                queries += encodeURIComponent( v.value );
            });

            // console.log(queries);
            return queries;
        },
        "requestSuccess": function requestSuccess( response ) {
            this.json = JSON.parse( response );
            window.Qast.config = this.json;
            this.buildPreview();

            if ( !this.hasFeeds ) {
                this.updateFeeds();
            }
            if ( !this.hasSources ) {
                this.updateSources();
            }
            this.buildCode();
            $(window).trigger("qast/form/update");
        },
        "buildPreview": function buildPreview() {
            var iframe = this.parseHTML( this.json.html );

            iframe = this.styleFrame( iframe );
            this.$preview.innerHTML = "";
            this.$preview.appendChild( iframe );
        },
        "buildCode": function buildCode() {
            this.$code.parentNode.parentNode.classList.add("is-expanded");
            this.$code.value = this.json.embed + "<script" + " async defer src='//" + location.hostname + (location.port ? ":" + location.port : "") + "/public/js/embed.js'></" + "script>";
            this.$twitter.value = "https://qast.fm/?" + this.serializeForm();
            this.$code.select();
        },
        "styleFrame": function styleFrame( iframe ) {
            iframe.width = "100%";
            iframe.frameborder = "0";
            iframe.style.display = "block";
            iframe.style.padding = "0";
            iframe.style.border = "none";
            iframe.style.height = "80px";
            iframe.style.maxWidth = "600px";
            iframe.style.marginRight = "auto";
            iframe.style.marginLeft = "auto";
            iframe.style.boxShadow = "0 1px 3px rgba(0,0,0,0.15)";

            return iframe;
        },
        "parseHTML": function parseHTML(str) {
            var tmp = document.implementation.createHTMLDocument();
            tmp.body.innerHTML = str;
            return tmp.body.children[0];
        },
        "updateFeeds": function updateFeeds( arr ) {
            this.$feeds.innerHTML = "";

            if ( typeof this.json.feed !== "string" ) {
                if ( typeof this.json.feeds === "object" && this.json.feeds.length > 1 ) {
                    var feedsObject = {
                        "feeds": []
                    };
                    this.json.feeds.forEach(function (v, i) {
                        var obj = {
                            "feed_label": v,
                            "feed_input": {
                                "@": {
                                    "value": i + 1
                                }
                            }
                        };
                        obj.feed_input["@"].checked = v === this.json.feed ? "checked": false;
                        feedsObject.feeds.push(obj);
                    }.bind(this));
                    this.$feeds.appendChild(this.template.feeds.render(feedsObject));
                } else {
                    this.$feeds.appendChild(this.template["no-feeds"].render(feedsObject));
                }
            }
            this.hasFeeds = true;
        },
        "updateSources": function updateSources() {
            this.$sources.innerHTML = "";
            if ( typeof this.json.enclosures === "object" && this.json.enclosures.length > 1 && this.json.soundcloud !== true) {
                var sourcesObject = {
                    "sources": []
                };
                this.json.enclosures.forEach(function (v, i) {
                    var obj = {
                        "source_label": v,
                        "source_input": {
                            "@": {
                                "value": i + 1
                            }
                        }
                    };
                    if ( i == 0 ) {
                        obj.source_input["@"].checked = "checked"
                    }
                    sourcesObject.sources.push(obj);
                });
                this.$sources.appendChild(this.template.sources.render(sourcesObject));
            }
            this.hasSources = true;
        }
    };

    window.QastEmbed = Embed;

    new Embed(document.getElementById("embed-form"));
}());
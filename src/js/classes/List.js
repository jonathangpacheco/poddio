/*requires: jQuery*/
var Poddio = Poddio || {};
Poddio.List = (function () {

    /**
    * Variables
    */
    var
        defaults = {
            el: "<ul>"
        };
    

    /**
    * Constructor
    */
    function List(config) {
        this.config = config;
        this.initVars();
    }

    /**
    * Instance methods and properties
    */

    // Init vars
    List.prototype.initVars = function () {
        this.NS = "PoddioList:";
        this.options = $.extend({}, defaults, this.config);
        this.resolveEl();
    };

    // Resolve el
    List.prototype.resolveEl = function () {
        this.$el = this.options.$el || $(this.options.el);
        this.el = this.$el[0];
    };

    // Trigger
    List.prototype.trigger = function (ev, data) {
        this.$el.trigger(this.NS + ev, data);
    };

    // Update
    List.prototype.update = function (data) {
        var
            i = 0,
            len = data.length;
        for (i; i < len; i += 1) {
            this.render(data[i], i === 0);
        }
    };

    // Render
    List.prototype.render = function (data, clear) {
        var html = "<li>";

        // Title
        html += data.title + "<br />";

        // Description
        html += data.description + "<br />";

        // Link
        html += "<a href='" + data.link + "' target='_blank'>" + data.link + "</a>";
        html += "</li>";

        if (clear) {
            this.$el.empty();
        }
        this.$el.append(html);
    };

    /**
    * Expose
    */
    return List;
}());

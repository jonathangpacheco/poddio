(function(document) {
    document.documentElement.className = document.documentElement.className.replace(new RegExp("(^|\\b)no-js(\\b|$)", "gi"), "js");
})(document);

/*!
loadCSS: load a CSS file asynchronously.
[c]2014 @scottjehl, Filament Group, Inc.
Licensed MIT
*/
function loadCSS(href, before, media, callback) {
    "use strict";
    // Arguments explained:
    // `href` is the URL for your CSS file.
    // `before` optionally defines the element we'll use as a reference for injecting our <link>
    // By default, `before` uses the first <script> element in the page.
    // However, since the order in which stylesheets are referenced matters, you might need a more specific location in your document.
    // If so, pass a different reference element to the `before` argument and it'll insert before that instead
    // note: `insertBefore` is used instead of `appendChild`, for safety re: http://www.paulirish.com/2011/surefire-dom-element-insertion/
    var ss = window.document.createElement("link");
    var ref = before || window.document.getElementsByTagName("script")[0];
    var sheets = window.document.styleSheets;
    ss.rel = "stylesheet";
    ss.href = href;
    // temporarily, set media to something non-matching to ensure it'll fetch without blocking render
    ss.media = "only x";
    // DEPRECATED
    if (callback) {
        ss.onload = callback;
    }
    // inject link
    ref.parentNode.insertBefore(ss, ref);
    // This function sets the link's media back to `all` so that the stylesheet applies once it loads
    // It is designed to poll until document.styleSheets includes the new sheet.
    ss.onloadcssdefined = function(cb) {
        var defined;
        for (var i = 0; i < sheets.length; i++) {
            if (sheets[i].href && sheets[i].href.indexOf(href) > -1) {
                defined = true;
            }
        }
        if (defined) {
            cb();
        } else {
            setTimeout(function() {
                ss.onloadcssdefined(cb);
            });
        }
    };
    ss.onloadcssdefined(function() {
        ss.media = media || "all";
    });
    return ss;
}

// Improved version of JavaScript fix for the iOS viewport scaling bug. See https://gist.github.com/901295
(function(doc) {
    "use strict";
    // Variables
    var addEvent = "addEventListener", type = "gesturestart", qsa = "querySelectorAll", scales = [ 1, 1 ], meta = qsa in doc ? doc[qsa]("meta[name=viewport]") : [];
    // Functions
    function fix() {
        meta.content = "width=device-width,minimum-scale=" + scales[0] + ",maximum-scale=" + scales[1];
        doc.removeEventListener(type, fix, true);
    }
    // Create
    if ((meta = meta[meta.length - 1]) && addEvent in doc) {
        fix();
        scales = [ .25, 1.6 ];
        doc[addEvent](type, fix, true);
    }
})(document);
module.exports = function ( grunt ) {

    // Project configuration.

    // Build = Local development
    // Dist = Motif distribution packages

    grunt.initConfig({
        pkg: grunt.file.readJSON("package.json"),

        // Folder Vars
        project: "<%= pkg.name %>",
        sourceDir: "",
        distDir: "<%= sourceDir %>dist/",
        buildDir: "<%= sourceDir %>build/",
        projectDir: "<%= sourceDir %>../../../",

        dist: {
            css: "<%= distDir %>css/",
            fonts: "<%= distDir %>fonts/",
            js: "<%= distDir %>js/"
        },

        build: {
            less: "<%= buildDir %>less/",
            css: "<%= buildDir %>css/",
            fonts: "<%= buildDir %>fonts/",
            js: "<%= buildDir %>js/"
        },

        source: {
            less: "<%= sourceDir %>styles/less/",
            js: "<%= sourceDir %>js/",
            fonts: "<%= sourceDir %>fonts/"
        },

        scripts: {
            groups: {
                home: [
                    "<%= source.js %>classes/Utils.js",
                    "<%= source.js %>vendor/jquery.js",
                    "<%= source.js %>vendor/nouislider.js",
                    "<%= source.js %>classes/temple.js",
                    "<%= source.js %>classes/Embed.js",
                    "<%= source.js %>home.js"
                ],
                inline: [
                    "<%= source.js %>vendor/hasJS.js",
                    "<%= source.js %>vendor/loadCSS.js",
                    "<%= source.js %>vendor/viewport.js"
                ],
                main: [
                    "<%= source.js %>vendor/modernizr.js",
                    "<%= source.js %>vendor/fastdom.js",
                    "<%= source.js %>classes/Utils.js",
                    "<%= source.js %>classes/UI.js",
                    "<%= source.js %>classes/AudioPlayer.js",
                    "<%= source.js %>main.js"
                ]
            },
            config: {
                build: {
                    // Build Global JS
                    "<%= build.js %><%= pkg.name %>.js": "<%= scripts.groups.main %>",
                    "<%= build.js %>home.js": "<%= scripts.groups.home %>",
                    "<%= source.js %>../views/partials/inline.js": "<%= scripts.groups.inline %>"
                },
                dist: {
                    // Dist Global JS
                    "<%= dist.js %><%= pkg.name %>.js": "<%= scripts.groups.main %>",
                    "<%= dist.js %>home.js": "<%= scripts.groups.home %>",
                    "<%= source.js %>../views/partials/_inline.jade": "<%= scripts.groups.inline %>"
                }
            }
        },

        // Tasks

        // Concat and Compress JS
        uglify: {
            build: {
                options: {
                    mangle: false,
                    beautify: true,
                    preserveComments: "all",
                    sourceMap: false,
                    report: "gzip",
                    compress: false
                },
                files: "<%= scripts.config.build %>"
            },
            dist: {
                options: {
                    mangle: true,
                    beautify: false,
                    sourceMap: false,
                    report: "gzip",
                    compress: true
                },
                files: "<%= scripts.config.build %>"
            }
        }
    });

    // Load the plugins that provide the tasks.


    grunt.loadNpmTasks("grunt-contrib-uglify");

    grunt.registerTask("stamp", "Create cache-busting stamp", function() {
        grunt.file.write( grunt.config("projectDir") + "src/resourcesVersion.php", "<?php $resources_version = " + Date.now() + "; ?>" );
    });

    // Default task, compiles LESS and JS into "build" folder
    grunt.registerTask("default", ["uglify:dist"]);

    // Run when you want to refresh everything
    grunt.registerTask("refresh", ["uglify:dist"]);
    grunt.registerTask("build", ["uglify:dist"]);
};

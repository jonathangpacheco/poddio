module.exports = (function () {
    var DB = {};
    DB.mongo = require("mongojs");
    DB.url = "localhost/poddio";
    DB.collections = [
        "users",
        "shows"
    ];
    return DB.mongo(DB.url, DB.collections);
}());

module.exports = (function () {

    /**
    * Imports
    */
    var
        Events = require("events").EventEmitter,
        cheerio = require("cheerio"),
        Utils = require("./Utils")
        SoundCloud = require("./SoundCloud");

    /**
    * Constructor
    */
    var GetMicrodata = function GetMicrodata( $, events ) {
            this.verbose = true;
            this.NS = "GetMicrodata";
            this.$ = $;
            this.events = events;
            this.blocks = false;
            this.files = [];
            this.config = {
                "soundcloud": false
            };

            this.init();
            return this;
        };

    /**
    * Instance methods and properties
    */
    GetMicrodata.prototype = {
        "init": function init() {
            this.getData();
        },
        "getData": function getData() {
            this.$("script[type='application/ld+json']").each(function eachDataBlock(i, elem) {
                var $elem = this.$(elem),
                    json = JSON.parse($elem.html());

                if ( json["@context"] === "http://schema.org" && json["@type"] === "Episode" ) {
                    if ( json.name ) {
                        this.config.title = json.name;
                    }
                    if ( json.image ) {
                        this.config.image = json.image;
                    }
                    if ( json.audio && json.audio["@type"] === "AudioObject" ) {
                        this.config.enclosure = json.audio.contentUrl;
                        this.config.enclosures = [ json.audio.contentUrl ];
                    }
                    return false;
                }

            }.bind(this));
            this.events.emit("merge", this.config, true);
        }
    };

    /**
    * Expose
    */
    return GetMicrodata;
}());

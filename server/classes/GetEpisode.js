module.exports = (function () {
    /**
     * 1. Get the source code for the passed URL
     * 2. Scrape the page looking for an audio file
     *     1. Look for <audio> tags
     *     2. Look for anchor tags with href values ending in ".mp3"
     *     3. Look for tags with data-url values ending in ".mp3"
     * 3. Grab the title form the <title> tag
     * 4. Save the audio file URL and the Title
     * ---------
     * 5. Look for a link to the RSS feed
     *     1. Look for the link rel="xml/rss"
     *     2. Look for anchor tags ending in rss-like strings
     * 6. Ensure the feed URL is absolute
     * 7. Fetch the feed URL
     * 8. If it's XML, get the Show Title, Show Image
     * 9. If we still don't have an audio file path, try to find the entry matching our URL
     * 10. If we find it, grab the enclosure
     * 11. Render a thumb of the Show Image
     * 12. Return the data
     */
    
    /*
    
        {
            "title": "Podcast Episode Title",
            "image": "http://cdn.example.com/images/podcast-image.jpg",
            "url": "http://example.com/podcast/4",
            "feed": "http://example.com/feed",
            "feeds": [
                "http://example.com/feed",
                "http://example.com/rss"
            ],
            "soundcloud": {
                "id": 188070,
                "objectUrl": "http://api.soundcloud.com/..."
            },
            "enclosure": "http://cdn.example.com/audio/podcast.mp3",
            "enclosures": [
                "http://cdn.example.com/audio/podcast.mp3",
                "http://cdn.example.com/audio/podcast-2.mp3"
            ],
            "start": false,
            "end": "00:07:23",
            "length": 100093
        }

    */

    /**
    * Imports
    */
    var
        Events = require("events").EventEmitter,
        request = require("request"),
        url = require("url"),
        config = require("./../config"),
        cheerio = require('cheerio'),
        sharp = require("sharp"),
        jade = require("jade"),
        Utils = require("./Utils"),
        SoundCloud = require("./SoundCloud"),
        GetFeed = require("./GetFeed"),
        GetMicrodata = require("./GetMicrodata"),
        GetPodcastDetails = require("./GetPodcastDetails"),
        GetAudioFile = require("./GetAudioFile");

    /**
    * Constructor
    */
    var GetEpisode = function GetEpisode( req, resp, json ) {
        this.json = json;
        this.initVars(req, resp);
        this.bindEvents();


        if ( req.query.url != null ) {
            this.getPageSource();
        } else {
            this.events.emit("return");
        }
    };

    /**
    * Instance methods and properties
    */
    GetEpisode.prototype = {
        "defaults": {
            "page": {
                "headers": {
                    "User-Agent": "node.js"
                }
            },
            "feed": {
                "method": "GET",
                "url": "/api/feed?url="
            },
            "jquery": "http://code.jquery.com/jquery-1.6.min.js",
            "feedStrings": [
                "/feed",
                "/feed/",
                "/feeds",
                "/feeds/",
                "/rss",
                "/rss/",
                "/podcast",
                "/podcast/",
                "feed.xsl",
                "rss.xsl"
            ],
            "detailsUrl": "/api/podcast/details"
        },
        "initVars": function initVars(req, resp) {
            this.verbose = true;
            this.NS = "GetEpisode";
            this.events = new Events();
            this.url = req.query.url || false;
            this.src = req.query.src || 1;
            this.feed = req.query.feed || 1;
            this.resp = resp;
            this.audioInfo = false;
            this.title = false;
            this.image = false;
            this.config = {
                "title": false,
                "image": false,
                "ogimage": false,
                "url": this.url,
                "feed": false,
                "feeds": [],
                "soundcloud": {},
                "enclosure": false,
                "enclosures": [],
                "length": false
            };

            if ( req.query.t ) {
                this.t = Utils.parseTimeQuery( req.query.t );
                this.config.start = this.t.start;
                this.config.end = this.t.end;
            } else {
                this.config.start = req.query.start || false;
                this.config.end = req.query.end || false;
            }

            this.log("Setting options");
            this.options = Utils.extend(this.defaults);
        },
        "buildDetailsRequest": function buildDetailsRequest() {
            var url = this.defaults.detailsUrl;

            if ( this.feed != null && this.feed !== "undefined" ) {
                url += "?url=";
                url += this.feed;
            }
            if ( this.config.soundcloud.objectUrl ) {
                url += "&sc=";
                url += this.config.soundcloud.objectUrl;
            }

            return url;
        },
        "bindEvents": function bindEvents() {
            this.events.on("return", function () {
                console.log("ON EVENT RETURN");
                if ( this.json && this.config.feed && this.config.enclosure ) {
                    // this.resp.send(this.config);
                    this.log(this.config.enclosure);

                    var data = new GetPodcastDetails({
                        "query": {
                            "file": true,
                            "url": this.config.feed,
                            "sc": this.config.soundcloud.objectUrl || false
                        }
                    }, this.resp);
                    data.events.on("return", function () {
                        // console.log("return");
                        this.config = this.mergeExternalConfig(data.config);
                        this.events.emit("render");
                    }.bind(this));
                    data.events.on("error", function () {
                        // console.log("error");
                        resp.status(500).send({ error: 'something blew up' });
                    });
                } else {
                    console.log("we should render");
                    this.events.emit("render");
                    // this.resp.render( "embed.jade", this.config );
                }
            }.bind(this));

            this.events.on("merge", function (obj, render) {
                this.log("Merging");
                this.mergeExternalConfig( obj );
                if ( render ) {
                    this.configEpisode();
                    // this.events.emit("return");
                }
            }.bind(this));
        },
        "getPageSource": function getPageSource() {
            this.log("Getting Page Source.");
            this.options.page.url = this.url;

            if ( Utils.isSoundcloud( this.options.page.url ) ) {
                this.log("It's a Soundcloud URL.");
                new SoundCloud( this.options.page.url, this.resp, this.events );
            } else {
                this.requestPage();
            }
        },
        "mergeExternalConfig": function mergeExternalConfig( obj ) {
            this.config = Utils.extend( this.config, obj );
            this.log(this.config);
            return this.config;
        },
        "requestPage": function requestPage() {
            this.log("About to get page");
            request( this.options.page, function onPageReturn(err, response, body) {

                if (err && response.statusCode !== 200) {
                    this.log("Request error.");
                } else {
                    this.log("Podcast page successfully retrieved.");
                }

                this.body = body;

                this.findAudioInformation();
            }.bind(this));
        },
        "feedStringSelectors": function feedStringSelectors() {
            var selector = "";
            this.options.feedStrings.forEach(function (i, v) {
                if ( v > 0 ) {
                    selector += ", ";
                }
                selector += "a[href$='";
                selector += i;
                selector += "']";
            });
            selector += ", a[href*='http://feeds.'], a[href*='https://feeds.'], a[href*='http://feed.'], a[href*='https://feed.']";
            return selector;
        },
        "getFeedUrl": function getFeedUrl( body, $ ) {

            var $head = $("head"),
                $feedLinks = $( this.feedStringSelectors() ),
                $rss = $head.find("link[type='application/rss+xml']"),
                feed = false;

            this.log("Feed Links");
            this.log($feedLinks.length);

            if ( $rss.length ) {
                feed = $rss.first().attr("href");
            } else if ( $feedLinks.length ) {
                feed = $feedLinks.first().attr("href");
            }

            if ( feed && feed.indexOf("http") !== 0 ) {
                var anchor = url.parse( this.url ),
                temp = "";

                // console.log(anchor);
                // 

                anchor.href = this.url;
                temp += anchor.protocol;

                if ( feed.indexOf("//") !== 0 ) {
                    temp += "//";
                    temp += anchor.host;
                }
                temp += feed;

                feed = temp;
            }

            this.log("Feed URL: " + feed);

            this.config.feed = feed;
            this.configEpisode();
        },
        "hasMicrodata": function hasMicrodata( $ ) {
            if ( $("script[type='application/ld+json']").length ) {
                return true;
            }
            return false;
        },
        "findAudioInformation": function findAudioInformation() {
            var $ = cheerio.load( this.body );

            if ( this.hasMicrodata( $ ) ) {
                new GetMicrodata( $, this.events );
            } else {
                this.audioInfo = new GetAudioFile( this.body, this.resp, this.events, this.url );

                if ( this.audioInfo.soundcloud !== true ) {
                    this.config.enclosures = this.audioInfo.enclosures;
                    if ( this.audioInfo.enclosures.length ) {
                        this.config.enclosure = this.src ? this.config.enclosures[ this.src - 1 ] : this.config.enclosures[ 0 ];
                    } else {
                        this.config.enclosure = false;
                    }
                    this.config.soundcloud = this.audioInfo.soundcloud;

                    var $ = cheerio.load( this.body ),
                        $title = $("title"),
                        $image = $("meta[property='og:image']");

                    if ( $title.length ) {
                        this.config.title = $title.text();
                        this.config.title = this.config.title.replace(/\u00a0/g, " ");
                        this.config.title = this.config.title.replace(/(\r\n|\n|\r)/gm," ");
                        this.config.title = this.config.title.replace(/\s+/g," ");
                        this.config.title.trim();
                    }

                    if ( $image.length ) {
                        this.config.ogimage = $image.attr("content");
                    }
                    this.config.feeds = new GetFeed( this.body, this.url );

                    this.config.feed = this.config.feeds.length ? this.feed && this.feed <= this.config.feeds.length && this.feed > 0 ? this.config.feeds[ this.feed - 1 ] : this.config.feeds[ 0 ] : false;
                    this.configEpisode();
                }
            }
        },
        "getLength": function getLength() {
            var start = !this.config.start ? 0 : Utils.convertToSeconds( this.config.start ),
                end = !this.config.end ? start + 600 : Utils.convertToSeconds( this.config.end );

            this.config.length = end - start;

            if ( this.config.length > 600 || this.config.length < 0 ) {
                if ( this.config.start ) {
                    end = start + 600;
                } else {
                    start = end - 600;
                }
                this.config.length = 600;
            }
            this.config.start = start;
            this.config.end = end;

            return this.config.length;
        },
        "configEpisode": function configEpisode() {
            this.log("Config Episode");

            this.config.length = this.getLength();
            this.config.lengthText = Utils.convertToString( this.config.length );
            this.config.startText = Utils.convertToString( this.config.start );
            this.config.endText = Utils.convertToString( this.config.end );

            // if ( this.json ) {
                this.config.src = this.src;
                var jsonConfig = Utils.extend(this.config);
                jsonConfig.pretty = false;
                jsonConfig.doctype = "html";
                jsonConfig.feed = this.feed;
                jsonConfig.domain = config.domain;
                var frame = jade.renderFile(__dirname + '/../../src/views/partials/_frame.jade', jsonConfig);
                var fallback = jade.renderFile(__dirname + '/../../src/views/partials/_fallback.jade', jsonConfig);
                this.config.html = frame;
                this.config.embed = fallback;
            // } else {
                this.config.encodedText = encodeURIComponent(this.config.title + "FullAudioRestart:0123456789");
                this.config.object = JSON.stringify( this.config );
            // }

            if ( this.config.ogimage ) {
                // @TODO: wait to resize image till we know we need it
                this.resizeImage( this.config.ogimage, "ogimage" );
            } else if ( this.config.image && !this.config.soundcloud ) {
                this.resizeImage( this.config.image, "image" );
            } else {
                this.events.emit("return");
            }
        },

        "resizeImage": function resizeImage( img, prop ) {
            request( {
                "headers": {
                    "User-Agent": "node.js"
                },
                "encoding": null,
                "url": img
            }, function onPageReturn(err, response, body) {
                console.log("On Image Return");

                sharp( body )
                    .resize(48, 48)
                    .min()
                    .crop(sharp.gravity.north)
                    .toFormat('jpeg')
                    .toBuffer(function (err, outputBuffer, info) {
                        if (err) {
                            this.log("Couldn't resize image: " + err);
                            this.events.emit("return");
                        } else {
                            this.log("Resized")
                            this.config[prop] = "data:image/" + info.format + ";base64," + outputBuffer.toString("base64");
                            this.events.emit("return");
                        }

                    }.bind(this));
            }.bind(this));

            
        },
        "log": function log() {
            if (this.verbose) {
                Array.prototype.unshift.call(arguments, " ".inverse.blue);
                console.log.apply(console, arguments);
            }
        }
    };

    /**
    * Expose
    */
    return GetEpisode;
}());

module.exports = (function () {

    /**
    * Imports
    */
    var
        Events = require("events").EventEmitter,
        cheerio = require("cheerio"),
        Utils = require("./Utils")
        SoundCloud = require("./SoundCloud");

    /**
    * Constructor
    */
    var GetAudioFile = function GetAudioFile( body, resp, events, url ) {
            this.verbose = true;
            this.NS = "GetAudioFile";
            this.events = events;
            this.resp = resp;
            this.body = body;
            this.url = url;
            this.$ = cheerio.load( this.body );
            this.files = [];
            this.soundCloud = false;

            return this.init();
        };

    /**
    * Instance methods and properties
    */
    GetAudioFile.prototype = {
        "init": function init() {
            var config = {};

            config.enclosures = this.findAudioFiles();
            config.soundcloud = this.isSoundcloud();

            console.log(config.enclosures);
            return config;
        },
        "isSoundcloud": function isSoundcloud() {
            return this.soundCloud;
        },
        "findAudioFiles": function findAudioFiles() {
            this.files = this.getSoundCloudFrame();
            this.files = this.getSoundCloudLink();
            this.files = this.getAudioTag();
            this.files = this.getDataUri();
            this.files = this.getDataUrl();
            this.files = this.getDataDownload();
            this.files = this.getMP3Link();

            return this.files;
        },
        "getAudioTag": function getAudioTag() {
            var $audio = this.$("audio");

            if ( $audio.length ) {
                Utils.log("Audio Tag Detected");

                $audio.each(function eachAudioTag( i, elem ) {
                    var $elem = this.$(elem),
                        file = false;

                    if ( $elem.attr("src") ) {
                        file = $elem.attr("src").trim();
                    } else if ( $elem.find("source[src*='.mp3']").length ) {
                        file = $elem.find("source[src*='.mp3']").attr("src").trim();
                    } else if ( $elem.find("source[type='audio/mpeg']").length ) {
                        file = $elem.find("source[type='audio/mpeg']").attr("src").trim();
                    }
                    file = Utils.makeFullPath( file, this.url, true );

                    if ( file && this.files.indexOf( file ) === -1 ) {
                        this.files.push( file );
                    }
                }.bind(this));
            }
            return this.files;
        },
        "getDataUri": function getDataUri() {
            var $audio = this.$("[data-uri*='.mp3']");

            if ( $audio.length ) {
                Utils.log("Data URI Detected");

                $audio.each(function eachAudioTag( i, elem ) {
                    var $elem = this.$(elem),
                        file = Utils.makeFullPath( $elem.data("uri").trim(), this.url, true );

                    if ( this.files.indexOf( file ) === -1 ) {
                        this.files.push( file );
                    }
                }.bind(this));
            }
            return this.files;
        },
        "getDataUrl": function getDataUrl() {
            var $audio = this.$("[data-url*='.mp3']");

            if ( $audio.length ) {
                Utils.log("Data URL Detected");

                $audio.each(function eachAudioTag( i, elem ) {
                    var $elem = this.$(elem),
                        file = Utils.makeFullPath( $elem.data("url").trim(), this.url, true );

                    if ( this.files.indexOf( file ) === -1 ) {
                        this.files.push( file );
                    }
                }.bind(this));
            }
            return this.files;
        },
        "getDataDownload": function getDataDownload() {
            var $audio = this.$("[data-download*='.mp3']");

            if ( $audio.length ) {
                Utils.log("Data Download Detected");

                $audio.each(function eachAudioTag( i, elem ) {
                    var $elem = this.$(elem),
                        file = Utils.makeFullPath( $elem.data("download").trim(), this.url, true );

                    if ( this.files.indexOf( file ) === -1 ) {
                        this.files.push( file );
                    }
                }.bind(this));
            }
            return this.files;
        },
        "getMP3Link": function getMP3Link() {
            var $audio = this.$("a[href*='.mp3']");

            if ( $audio.length ) {
                Utils.log("MP3 Link Detected");

                $audio.each(function eachAudioTag( i, elem ) {
                    var $elem = this.$(elem),
                        file = Utils.makeFullPath( $elem.attr("href").trim(), this.url, true );

                    if ( this.files.indexOf( file ) === -1 ) {
                        this.files.push( file );
                    }
                }.bind(this));
            }
            return this.files;
        },
        "getSoundCloudLink": function getSoundCloudLink() {
            var $audio = this.$("a[href*='soundcloud.com'][href*='/download']");

            if ( $audio.length ) {
                Utils.log("SoundCloud Link Detected");

                this.soundCloud = true;

                new SoundCloud( $audio.first().attr("href").trim(), this.resp, this.events, true);

            }
            return this.files;
        },
        "getSoundCloudFrame": function getSoundCloudFrame() {
            var $audio = this.$("iframe[src*='soundcloud.com/tracks'], iframe[src*='soundcloud.com%2Ftracks'], iframe[src*='soundcloud.com%2ftracks']"),
                scFrames = [];

            if ( $audio.length ) {
                Utils.log("SoundCloud Frame Detected");

                this.soundCloud = true;

                new SoundCloud( $audio.first().attr("src").trim(), this.resp, this.events, true);
            }
            return this.files;
        }
    };

    /**
    * Expose
    */
    return GetAudioFile;
}());

module.exports = (function () {

    // @TODO: emit event to return config rather than rendering it here
    var
        request = require("request"),
        Utils = require("./Utils"),
        SoundCloud = function SoundCloud( url, resp, events, frame ) {
            this.url = url;
            this.resp = resp;
            this.events = events;
            this.frame = frame;
            this.init();

            return this;
        };

    SoundCloud.prototype = {
        "settings": {
            "url": {
                "base": "https://api.soundcloud.com/"
            },
            "id": "6bd0e7366dd6b2b0b55b4fb1f5e87758"
        },
        "verbose": true,
        "init": function init() {
            Utils.log("Initializing SoundCloud");

            Utils.log("Is part of frame: " + this.frame);
            if ( this.frame ) {
                this.getTrackObjectFromID( this.getTrackID( this.url ) );
            } else {
                this.getTrackObjectFromURL();
            }
        },
        "configEpisode": function configEpisode( json ) {
            Utils.log("Configuring Episode");

            var config = {
                "title": json.title,
                "image": json.user && json.user.avatar_url ? json.user.avatar_url : json.artwork_url,
                "soundcloud": true,
                "enclosure": json.streamable ? this.attachClientID(json.stream_url) : this.attachClientID(json.download_url),
                "enclosures": [
                    this.attachClientID(json.stream_url),
                    this.attachClientID(json.download_url)
                ]
            };
            if ( this.frame == null ) {
                config.url = this.url;
            }
            // config.object = JSON.stringify( config );

            Utils.log(config);
            this.events.emit("merge", config, true);
        },
        "getTrackID": function getTrackID( url ) {
            Utils.log("Getting Track ID from: " + url);

            var start = url.indexOf("/tracks/") + 8,
                stop = url.length,
                id;

            Utils.log("Position of `/tracks/`: " + start);

            if ( start < 0 ) {
                Utils.error("Could Not Find ID in URL.");
                this.getTrackObjectFromURL();
                return false;
            } else {


                if ( start < 8 ) {
                    start = url.indexOf("%2Ftracks%2") + 12;
                }

                if ( start < 12 ) {
                    Utils.error("Could Not Find ID in URL.");
                }

                if ( url.indexOf("%3F", start) > -1 ) {
                    stop = url.indexOf("%3F", start) < stop ? url.indexOf("%3F", start) : stop;
                }
                if ( url.indexOf("%3f", start) > -1 ) {
                    stop = url.indexOf("%3f", start) < stop ? url.indexOf("%3f", start) : stop;
                }
                if ( url.indexOf("?", start) > -1 ) {
                    stop = url.indexOf("?", start) < stop ? url.indexOf("?", start) : stop;
                }
                if ( url.indexOf("&", start) > -1 ) {
                    stop = url.indexOf("&", start) < stop ? url.indexOf("&", start) : stop;
                }

                id = parseInt(url.substring(start, stop), 10);

                Utils.log("Track ID: " + id);

                if ( isNaN(id) ) {
                    Utils.log("Track ID is not a number.");
                    this.getTrackObjectFromURL();
                    return false;
                }

                return id;
            }
        },
        "getObjectURLFromID": function getObjectURLFromID( id ) {
            return this.settings.url.base + "tracks/" + id + ".json?client_id=" + this.settings.id;
        },
        "getObjectURLFromURL": function getObjectURLFromURL() {
            var url;
            if ( this.url.substring( this.url.length - 9) === "/download" ) {
                url = this.url.substring( 0, this.url.indexOf("/download") );
            } else if ( this.url.substring( this.url.length - 10) === "/download/" ) {
                url = this.url.substring( 0, this.url.indexOf("/download/") );
            } else {
                url = this.url;
            }
            return this.settings.url.base + "resolve.json?url=" + url + "&client_id=" + this.settings.id;
        },
        "getTrackObjectFromID": function getTrackObjectFromID( id ) {
            Utils.log("Getting Track Object from SoundCloud ID: " + id);

            if ( !id ) {
                return false;
            }

            var requestUrl = this.getObjectURLFromID( id );

            Utils.log(requestUrl);

            this.getTrackObject( requestUrl );
        },
        "getTrackObjectFromURL": function getTrackObjectFromURL() {
            Utils.log("Getting Track Object from SoundCloud URL");

            var requestUrl = this.getObjectURLFromURL();

            Utils.log(requestUrl);

            this.getTrackObject( requestUrl );
        },
        "getTrackObject": function getTrackObject( url ) {
            request({
                "url": url
            }, function (error, response, body) {
                if (!error && response.statusCode === 200) {
                    this.configEpisode( JSON.parse(body) );
                } else {
                    Utils.error("There was an error getting the SoundCloud Object: " + error);
                }
            }.bind(this));
        },
        "getTrackDownloadURL": function getTrackObjectURL( id ) {
            return this.settings.url.base + "tracks/" + id + "/download?client_id=" + this.settings.id;
        },
        "attachClientID": function attachClientID( url ) {
            return url + "?client_id=" + this.settings.id;
        },
        "isSoundCloud": function isSoundCloud( url ) {
            if ( url.indexOf("soundcloud.com/") > -1 ) {
                return true;
            }
            return false;

        }
    };

    return SoundCloud;

}());

module.exports = (function () {

    /**
    * Imports
    */
    var
        Events = require("events").EventEmitter,
        request = require("request"),
        FeedParser = require("./FeedParser"),
        http = require("http");

    /**
    * Constructor
    */
    function PodcastData(url) {
        this.NS = "PodcastData";
        this.url = url;
        this.events = new Events();
        this.get();
    }

    /**
    * Instance methods and properties
    */
    PodcastData.prototype.get = function () {
        var self = this,
            options = {
                "method": "GET",
                "url": self.url,
                "headers": {
                    'user-agent': 'node.js'
                }

            };

        console.log("Getting Podcast Data");

        // http.get( self.url, function ( resp ) {
        //     resp.addListener( "end", function () {
        //         self.parse( feed );
        //     });
        //     resp.charset = 'utf-8';
        //     resp.on( "data", function ( data ) {
        //         feed += data;
        //     });
        // });
        self.request = request( options, function (error, response, body) {
            console.log(error);
            // console.log(response);
            // console.log(body);
            if ( error ) {
                console.log("Podcast Data Failure: " + error);
                // console.log(body);
            } else {
                self.parse( body );
            }
        });
    };

    PodcastData.prototype.parse = function ( feed ) {
        var self = this,
            cleanedFeed = feed.replace("\ufeff", "");

        FeedParser.parse( cleanedFeed, { "trim": true }, function ( e, parsed ) {
            if ( e == null ) {
                self.rss = parsed.rss;
                self.events.emit("parse");
            } else {
                console.log(e);
                self.events.emit("error");
            }
        });
    };

    /**
    * Expose
    */
    return PodcastData;
}());

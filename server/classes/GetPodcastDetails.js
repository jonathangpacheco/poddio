module.exports = (function () {

    /**
    * Imports
    */
    var
        fs = require("fs"),
        http = require("http"),
        config = require("./../config"),
        Events = require("events").EventEmitter,
        express = require("express"),
        request = require("request"),
        jsdom = require("jsdom"),
        sharp = require("sharp"),
        httpProxy = require('http-proxy'),
        PodcastData = require("./PodcastData"),
        RenderConfig = require("./RenderConfig");

    /**
    * Constructor
    */
    var extend = function extend() {
            var newObject = {},
                i,
                key;
            for ( i = 0; i < arguments.length; i += 1 ) {

                for ( key in arguments[i] ) {
                    try {
                        // Property in destination object set; update its value.
                        if ( arguments[i][key].constructor === Object ) {
                            newObject[key] = extend( newObject[key], arguments[i][key] );
                        } else {
                            newObject[key] = arguments[i][key];
                        }
                    } catch ( e ) {
                        // Property in destination object not set;
                        // create it and set its value.
                        newObject[key] = arguments[i][key];
                    }
                }
            }
            return newObject;
        },
        GetPodcastDetails = function GetPodcastDetails( req, resp ) {
            // console.log(req);
            this.verbose = true;
            this.NS = "GetPodcastDetails";
            this.url = req.query.url || false;
            this.events = new Events();
            this.file = req.query.file || false;
            this.sc = req.query.sc || false;
            // this.title = req.query.title || false;
            this.episode = req.query.episode || false;
            this.resp = resp;
            this.log("Setting options");
            this.options = extend(this.defaults);
            this.show = false;
            this.image = false;
            this.item = false;
            this.config = {};
            // console.log(this.url);
            this.getFeedData();
        };

    /**
    * Instance methods and properties
    */
    GetPodcastDetails.prototype = {
        "defaults": {
            "page": {
                "headers": {
                    "user-agent": "node.js"
                }
            },
            "feed": {
                "method": "GET",
                "url": "https://" + config.domain + "/api/feed?url=",
                "headers": {
                    "user-agent": "node.js"
                },
                "port": 443
            }
        },

        "getFeedData": function getFeedData() {
            
            var config = extend( this.options.feed );

            if ( this.url != null ) {
                // console.log(this.url);
                config.url += this.url;

                var data = new PodcastData(this.url);
                data.events.on("parse", function () {
                    if ( data.rss === "" || data.rss == null ) {
                        this.events.emit("error");
                    } else {
                        console.log(data.rss);
                        var returnedBody = data.rss;
                        if ( returnedBody.hasOwnProperty("error")) {
                            console.log("Doesn't start with {");
                            this.events.emit("return");
                        } else {
                            this.log("Successfuly retrieved RSS feed.")

                            if ( returnedBody ) {
                                this.body = returnedBody;

                                if ( !this.file ) {
                                    this.findEpisode();
                                } else {
                                    this.configEpisode();
                                }
                            } else {
                                this.log("RSS Feed body is undefined.");
                                this.events.emit("return");
                            }
                        }
                    }
                    console.log(data.rss);
                    // resp.send(data.rss);
                }.bind(this));
                data.events.on("error", function () {
                    console.log("error");
                    this.events.emit("return");
                    // resp.status(500).send({ error: 'something blew up' });
                }.bind(this));
            }


            // var config = extend( this.options.feed );

            // if ( this.url != null ) {
            //     // console.log(this.url);
            //     // config.url += this.url;
            //     config.agentOptions = {
            //         key: fs.readFileSync(__dirname + '/../../qast.fm.key'),
            //         cert: fs.readFileSync(__dirname + '/../../qast_fm.crt'),

            //         // This is necessary only if the client uses the self-signed certificate.
            //         ca: [ fs.readFileSync(__dirname + '/../../qast_fm.ca-bundle') ]
            //     };
            //     console.log(config);
                
            //     // console.log(config);

            //     request( config, function (error, response, body) {
            //         var returnedBody;

            //         console.log("Get Feed Data Request Results");
            //         console.log(error);
            //         console.log(response);
            //         console.log(body);

            //         if ( error || body === "" ) {
            //             this.log("Error retrieving the feed code.");
            //             if ( error ) {
            //                 this.log("Error: " + error);
            //             }
            //             this.events.emit("return");
            //         } else {
            //             returnedBody = JSON.parse( body );
            //             if ( returnedBody.hasOwnProperty("error")) {
            //                 console.log("Doesn't start with {");
            //                 this.events.emit("return");
            //             } else {
            //                 this.log("Successfuly retrieved RSS feed.")

            //                 if ( returnedBody ) {
            //                     this.body = returnedBody;

            //                     if ( !this.file ) {
            //                         this.findEpisode();
            //                     } else {
            //                         this.configEpisode();
            //                     }
            //                 } else {
            //                     this.log("RSS Feed body is undefined.");
            //                     this.events.emit("return");
            //                 }
            //             }
            //         }
            //     }.bind(this) );
            // }
        },
        "findEpisode": function findEpisode() {
            this.log("Finding episode within RSS feed.");
            this.log("Looking for: " + this.episode);

            this.config.enclosures = [];

            this.body.channel[ 0 ].item.forEach(function eachFeedItem(v) {
                if ( v.link[ 0 ].indexOf(this.episode) === 1 ) {
                    this.log("Match Found.");
                    this.config.enclosures.push( v.enclosure[ 0 ].$.url );
                }
            }.bind(this) );
            this.configEpisode();
        },
        "configEpisode": function configEpisode() {
            this.log("Configuring the episode details.");

            if ( !this.file ) {
                this.config.enclosure = this.config.enclosures.length ? this.config.enclosures[ 0 ] : false;
            }

            this.log("SoundCloud? " + this.sc);
            if ( this.sc ) {
                request( { "url": this.sc }, function (error, response, body) {
                    var returnedBody = JSON.parse( body );

                    if ( returnedBody.streamable ) {
                        this.config.file = returnedBody.stream_url + "?client_id=6bd0e7366dd6b2b0b55b4fb1f5e87758";
                    } else {
                        this.config.file = returnedBody.download_url + "?client_id=6bd0e7366dd6b2b0b55b4fb1f5e87758";
                    }


                    if ( returnedBody.user && returnedBody.user.avatar_url ) {
                        this.config.image = returnedBody.user.avatar_url;
                    } else {
                        this.config.image = returnedBody.artwork_url;
                    }

                    if ( this.config.image ) {
                        // this.resizeImage( this.config.image );
                        // 
                        this.config.image = this.config.image;
                        this.events.emit("return");
                    } else {
                        this.events.emit("error");
                    }
                }.bind(this) );
            } else {
                if ( this.body.channel[ 0 ][ "itunes:image" ] ) {
                    this.config.image = this.body.channel[ 0 ][ "itunes:image" ][ 0 ].$.href;
                } else if ( this.body.channel[ 0 ].image ) {
                    this.config.image = this.body.channel[ 0 ].image[ 0 ].url[ 0 ];
                } else {
                    this.config.image = false;
                }

                if ( this.config.image ) {
                    this.resizeImage( this.config.image );
                } else {
                    this.events.emit("return");
                }
            }

        },
        "resizeImage": function resizeImage( img ) {
            request( {
                "headers": {
                    "User-Agent": "node.js"
                },
                "encoding": null,
                "url": img
            }, function onPageReturn(err, response, body) {

                sharp( body )
                    .resize(48, 48)
                    .min()
                    .crop(sharp.gravity.north)
                    .toFormat('jpeg')
                    .toBuffer(function (err, outputBuffer, info) {
                        if (err) {
                            this.log("Couldn't resize image: " + err);
                            this.events.emit("return");
                        } else {
                            this.config.image = "data:image/" + info.format + ";base64," + outputBuffer.toString("base64");
                            this.events.emit("return");
                        }

                    }.bind(this));
            }.bind(this));
        },
        "log": function log() {
            if (this.verbose) {
                Array.prototype.unshift.call(arguments, " ".inverse.blue);
                console.log.apply(console, arguments);
            }
        }
    };

    /**
    * Expose
    */
    return GetPodcastDetails;
}());

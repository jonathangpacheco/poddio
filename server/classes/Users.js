module.exports = (function () {

    /**
    * Imports
    */
    var
        Events = require("events").EventEmitter,
        url = require("url"),
        db = require("../db.js"),
        ObjectID = db.ObjectId;

    /**
    * Constructor
    */
    function Users(url) {
        this.NS = "Users";
        this.url = url;
        this.events = new Events();
    }

    /**
    * Instance methods and properties
    */
    
    // Get
    Users.prototype.get = function (obj, fn) {
        return db.users.find(obj, fn);
    };
    
    // Get
    Users.prototype.getOnly = function (obj, fn) {
        return db.users.findOne(obj, fn);
    };
    
    // Get
    Users.prototype.getByID = function (id, fn) {
        return this.get({ _id: ObjectID(id) }, fn);
    };

    // Set/Update
    Users.prototype.set = function (obj, fn) {
        return db.users.update(obj, fn);
    };

    // Create
    Users.prototype.create = function (obj, fn) {
        return db.users.save(obj, fn);
    };

    // Exists
    Users.prototype.exists = function (obj, fn) {
        this.get(obj, function (err, vals) {
            var exists = false;

            if ( vals.length ) {
                exists = true;
            } else {
                exists = false;
            }
            
            fn.call( this, exists );
        });
    };

    Users.prototype.new = function ( body, resp, fn ) {
        var self = this,
            newUser;

        self.exists({
            username: body.username
        }, function ( exists ) {
            if ( !exists ) {
                self.create({
                    username: body.username,
                    password: body.password
                }, function (err, vals) {
                    if (err || !vals) {
                        resp.statusCode = 500;
                        resp.send("Internal Error: " + err);
                    } else {
                        fn.call( self, vals, function ( error ) {
                            if (error) {
                                throw error;
                            }
                        });
                    }
                });
            } else {
                resp.send(false);
            }
        });
    };

    Users.prototype.post = function (req, resp) {
        var body = req.body.username ? req.body : false,
            urlParts;

        if ( !body ) {
            urlParts = url.parse( req.url, true );
            body = urlParts.query;
        }

        if (body.username && body.password) {
            this.new( body, resp, function ( user ) {
                resp.send(user);
            } );
        } else {
            resp.statusCode = 400;
            resp.send("Bad Request: Invalid parameters");
        }
    };

    /**
    * Expose
    */
    return Users;
}());

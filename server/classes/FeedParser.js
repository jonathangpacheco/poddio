var fs = require("fs"),
    xml2js = require("xml2js"),
    parser;

module.exports = {
    parse: function parseXML ( xml, options, callback ) {
        parser = new xml2js.Parser( options );
        parser.parseString(xml, function (err, result) {
            callback( err, result );
        });
        // parser.addListener( "end", function ( result ) {
        //     // console.log(result);
        //     console.log("end");
        // });
        // parser.addListener( "error", function ( err ) {
        //     callback( err );
        // });

        // try {
        //     parser.parseString( xml );
        // } catch( err ) {
        //     callback( err );
        // }
    } 
};

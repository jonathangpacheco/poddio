module.exports = (function () {

    /**
    * Vars
    */ 
    var

        // Imports
        http = require("http"),
        https = require("https"),
        path = require("path"),
        config = require("./../config"),
        express = require("express"),
        fs = require("fs"),
        compression = require("compression"),
        bodyParser = require('body-parser'),
        colors = require("colors"),
        passport = require('passport'),
        request = require("request"),
        httpProxy = require('http-proxy'),
        stylus = require('stylus'),
        favicon = require('serve-favicon'),
        LocalStrategy = require('passport-local').Strategy,

        // Classes
        Users = require("./Users"),
        Accounts = require("./Accounts"),
        PodcastData = require("./PodcastData"),
        GetEpisode = require("./GetEpisode"),
        GetPodcastDetails = require("./GetPodcastDetails"),
        ShowFeed = require("./ShowFeed"),
        RenderConfig = require("./RenderConfig"),
        proxy = httpProxy.createProxyServer({});

    /**
    * Constructor
    */
    function App() {
        this.initVars();
        this.initSettings();
        this.initViews();
        this.initRoutes();
        this.listen();
    }

    /**
    * Instance methods and properties
    */

    // Init vars
    App.prototype.initVars = function () {
        var tlsSettings = {
            key: fs.readFileSync(__dirname + '/../../qast.fm.key'),
            cert: fs.readFileSync(__dirname + '/../../qast_fm.crt'),
            ca: [
                fs.readFileSync(__dirname + '/../../comodorsadomainvalidationsecureserverca.crt'),
                fs.readFileSync(__dirname + '/../../comodorsaaddtrustca.crt')
            ]
        };
        this.verbose = true;
        this.users = new Users();
        this.accounts = new Accounts();
        this.port = process.env.PORT || config.ports.http;
        this.routesConfig = new RenderConfig();
        this.server = express();
        https.createServer(tlsSettings, this.server).listen(config.ports.https);
        this.log("Server created");
    };

    // Init settings
    App.prototype.initSettings = function () {
        var self = this;

        // Add a handler to inspect the req.secure flag (see 
        // http://expressjs.com/api#req.secure). This allows us 
        // to know whether the request was via http or https.
        this.server.all("*", function (req, res, next) {
            if (req.secure) {
                // request was via https, so do no special handling
                next();
            } else {
                // request was via http, so redirect to https
                res.redirect('https://' + req.headers.host + req.url);
            }
        });

        // this.server.use(compression());
        // this.server.use(bodyParser.json());
        // this.server.use(express.cookieParser());
        // this.server.use(
        //     express.session({
        //         key: 'express.sid',
        //         secret: 'keyboard cat'
        //     })
        // );
        // // this.server.use(express.session());
        // this.server.use(passport.initialize());
        // this.server.use(passport.session());

        // passport.use( this.localStrategy() );

        // passport.serializeUser( function ( user, done ) {
        //     done( null, user._id );
        // });

        // passport.deserializeUser( function ( id, done ) {
        //     self.users.getByID( id, function ( err, user ) {
        //         done( err, user );
        //     });
        // });

        this.log("Initialized middleware");
    };

    App.prototype.localStrategy = function () {
        var self = this;

        return new LocalStrategy( function ( username, password, done ) {
            self.users.getOnly({
                username: username
            }, function ( err, user ) {
                if (err) {
                    return done( err );
                }
                if ( !user ) {
                    return done( null, false, {
                        message: "Incorrect username."
                    });
                }
                if ( password !== user.password ) {
                    return done( null, false, {
                        message: "Incorrect password."
                    });
                }
                return done( null, user );
            });
        });
    };

    // Log
    App.prototype.log = function () {
        if (this.verbose) {
            Array.prototype.unshift.call(arguments, " ".inverse.blue);
            console.log.apply(console, arguments);
        }
    };

    // Error Log
    // _TODO: Combine with normal log
    App.prototype.errorLog = function () {
        if (this.verbose) {
            Array.prototype.unshift.call(arguments, " ".inverse.red);
            console.log.apply(console, arguments);
        }
    };

    App.prototype.restrict = function(req, resp, next) {
        if ( req.isAuthenticated() ) {
            next();
        } else {
            // req.session.error = 'Access denied!';
            resp.redirect('/');
        }
    };

    // Init routes
    App.prototype.initRoutes = function () {
        var self = this;

        /**
        * Assets and pages
        */

        // Static
        self.server.use( "/public/js", express.static(__dirname + "/../../src/build/js") );
        self.server.use( "/styles", express.static(__dirname + "/../../src/styles") );
        self.server.use( "/images", express.static(__dirname + "/../../src/images") );
        self.server.get( "/", function ( req, resp ) {
            if ( req.query.url ) {
                var data = new GetEpisode( req, resp, true );
            } else {
                var data = new GetEpisode({
                    "query": {
                        "url": "https://www.theincomparable.com/theincomparable/150/index.php",
                        "start": "52:22",
                        "end": "1:02:19"
                    }
                }, resp, true );
            }
            data.events.on("render", function () {
                data.config.domain = config.domain;
                resp.render("index.jade", data.config);
            });
            data.events.on("error", function () {
                console.log("error");
                resp.status(500).send({ error: 'something blew up' });
            });
        });

        self.server.get("/file", function ( req, resp ) {
          if (req.query.url) {
            console.log(req.query.url);
            var x = request( req.query.url);
            req.pipe(x);
            x.pipe(resp);
          }
        });

        // Search
        self.server.get("/feed", function ( req, resp ) {
            var feed = new ShowFeed( req, resp );
        });


        self.server.get("/embed.json", function ( req, resp ) {
            var data = new GetEpisode(req, resp, true);
            data.events.on("render", function () {
                resp.send( data.config );
            });
            data.events.on("error", function () {
                console.log("error");
                resp.status(500).send({ error: 'something blew up' });
            });

        });
        self.server.get("/embed", function ( req, resp ) {
            var data = new GetEpisode(req, resp);
            data.events.on("render", function () {
                data.config.domain = config.domain;
                resp.render("embed.jade", data.config);
            });
            data.events.on("error", function () {
                console.log("error");
                resp.status(500).send({ error: 'something blew up' });
            });
        });

        /**
        * Users and Accounts
        *

        // Create User Account (GET)
        self.server.get("/account/create", function ( req, resp ) {
            resp.render( self.routesConfig.accountCreate.tmpl, self.routesConfig.accountCreate.options );
        });

        // Create User Account (POST)
        self.server.post("/account/create", function (req, resp) {
            self.accounts.create( req, resp,
                function () {
                    resp.redirect("/test");
                },
                function () {
                    resp.redirect("/account/create");
                }
            );
        });

        // Log In (POST)
        self.server.post("/login",
            passport.authenticate("local", {
                successRedirect: "/test",
                failureRedirect: "/"
            })
        );

        // Log Out
        self.server.get("/logout", function (req, resp) {
          req.logout();
          resp.redirect("/");
        });

        // TEST
        self.server.get("/testee", self.restrict, function (req, resp) {
          resp.redirect("/test");
        });
        self.log("Static routes created");

        // Dynamic
        self.server.get("/:page", function (req, resp) {
            resp.render(req.params.page + ".jade", self.routesConfig.page.options);
        });
        self.log("Dynamic routes created");

        /**
        * API
        */

        // Parse
        self.server.get("/api/feed", function (req, resp) {
            var data = new PodcastData(req.query.url);
            data.events.on("parse", function () {
                resp.send(data.rss);
            });
            data.events.on("error", function () {
                console.log("error");
                resp.status(500).send({ error: 'something blew up' });
            });
        });

        // Parse
        self.server.get("/api/podcast/details", function (req, resp) {
            var data = new GetPodcastDetails(req);
            data.events.on("return", function () {
                console.log("return");
                resp.send(data.config);
            });
            data.events.on("error", function () {
                console.log("error");
                resp.status(500).send({ error: 'something blew up' });
            });
        });

        // Parse
        // self.server.get("/api/podcast/file", function (req, resp) {
        //     var newurl = req.query.url;
        //     request({
        //         "headers": {
        //             "user-agent": "node.js"
        //         },
        //         "url": newurl
        //     }).pipe(resp);
        // });

        // User - create
        // self.server.post("/api/user", function (req, resp) {
        //     self.users.post(req, resp);
        // });

        this.log("API routes created");
    };

    // Init views
    App.prototype.initViews = function () {
        this.server.set('view engine', 'jade');
        this.server.set("views", __dirname + "/../../src/views");
        this.server.use(favicon(__dirname + '/../../src/images/favicon.ico')); 
        this.log("View engine set to 'jade'");
    };

    // Listen
    App.prototype.listen = function () {

        // Expose to port
        this.server.listen(this.port);
        this.log("Server listening on port " + this.port);
    };

    // Expose
    return App;
}());

module.exports = (function () {
    var RenderConfig = {
        "index": {
            "tmpl": "index.jade",
            "options": {
                "title": "Welcome"
            }
        },
        "accountCreate": {
            "tmpl": "account/create.jade",
            "options": {
                "title": "Create an Account"
            }
        },
        "page": {
            "options": {
                "title": "Radio for Smart People"
            }
        },
        "feed": {
            "tmpl": "show/listing.jade",
            "options": {
                "title": "Show Feed"
            }
        }
    };
        
    return RenderConfig;
});

module.exports = (function () {
    "use strict";

    var url = require("url"),
        config = require("./../config"),
        Utils = {
        "extend": function extend() {
            var newObject = {},
                i,
                key;

            for ( i = 0; i < arguments.length; i += 1 ) {

                for ( key in arguments[i] ) {
                    try {
                        // Property in destination object set; update its value.
                        if ( arguments[i][key].constructor === Object ) {
                            newObject[key] = Utils.extend( newObject[key], arguments[i][key] );
                        } else {
                            newObject[key] = arguments[i][key];
                        }
                    } catch ( e ) {
                        // Property in destination object not set;
                        // create it and set its value.
                        newObject[key] = arguments[i][key];
                    }
                }
            }
            return newObject;
        },
        "isSoundcloud": function isSoundcloud( url ) {
            this.log("Checking for Soundcloud URL.");
            if ( url.indexOf("soundcloud.com/") > -1 ) {
                return true;
            }
            return false;
        },
        "log": function log() {
            Array.prototype.unshift.call(arguments, " ".inverse.blue);
            console.log.apply(console, arguments);
        },
        "error": function error() {
            Array.prototype.unshift.call(arguments, " ".inverse.red);
            console.log.apply(console, arguments);
        },
        "parseTimeQuery": function parseTimeQuery( q ) {
            var start,
                end,
                arr;

            if ( q.indexOf(",") < 0 ) {
                start = q;
                end = false;
            } else {
                arr = q.split(",");
                start = arr[0];
                end = arr[1];
            }

            start = start === "" ? false : start;
            end = end === "" ? false : end;

            return {
                "start": start,
                "end": end
            };
        },
        "makeFullPath": function makeFullPath( path, orig, proxy ) {
            this.log("Making sure URL is full-path.");

            if ( path && path.indexOf("http") !== 0 ) {
                var anchor = url.parse( orig ),
                    temp = "";

                anchor.href = orig;
                temp += anchor.protocol;

                if ( path.indexOf("//") !== 0 ) {
                    temp += "//";
                    temp += anchor.host;
                }
                temp += path;

                path = temp;
            } else if ( path && proxy ) {
                console.log("ELSE", path);
                var anchor = url.parse( path ),
                    temp = "";

                anchor.href = path;
                console.log(anchor.protocol);

                if ( anchor.protocol === "http:" ) {
                    temp += "https://";
                    temp += config.domain;
                    temp += "/file?url=";
                }
                temp += path;
                path = temp;
            }

            this.log(path);
            return path;
        },
        // "convertToSeconds": function convertToSeconds( time ) {
        //     var timeArray = time.split(":"),
        //         hours = 0,
        //         minutes = 0,
        //         seconds = 0,
        //         totalSeconds = 0;

        //     if ( timeArray.length === 3 ) {
        //         hours = parseFloat( timeArray[ 0 ] );
        //         minutes = parseFloat( timeArray[ 1 ] );
        //         seconds = parseFloat( timeArray[ 2 ] );
        //     } else if ( timeArray.length === 2 ) {
        //         minutes = parseFloat( timeArray[ 0 ] );
        //         seconds = parseFloat( timeArray[ 1 ] );
        //     } else if ( timeArray.length === 1 ) {
        //         seconds = parseFloat( timeArray[ 0 ] );
        //     }
        //     totalSeconds += hours * 60 * 60;
        //     totalSeconds += minutes * 60;
        //     totalSeconds += seconds;

        //     return totalSeconds;
        // },
        "convertToSeconds": function convertToSeconds( time ) {
            // https://github.com/davatron5000/TimeJump/blob/master/timeJump.js#L36
            var plain = /^\d+(\.\d+)?$/g,
                npt = /^(?:npt:)?(?:(?:(\d+):)?(\d\d?):)?(\d\d?)(\.\d+)?$/,
                quirks = /^(?:(\d\d?)[hH])?(?:(\d\d?)[mM])?(\d\d?)[sS]$/,
                match;

            if ( plain.test(time) ) {
                return parseFloat( time );
            }
            match = npt.exec( time ) || quirks.exec( time );
            if (match) {
                return ( 3600 * (parseInt(match[1],10) || 0) + 60 * (parseInt(match[2],10) || 0) + parseInt(match[3],10) + (parseFloat(match[4]) || 0) );
            }
            return 0;
        },
        "convertToString": function convertToString( time ) {
            var hours = Math.floor(time / 3600),
                minutes,
                seconds,
                stamp = "";

            time -= hours * 3600;

            minutes = Math.floor(time / 60);
            time -= minutes * 60;

            seconds = parseInt(time % 60, 10);

            if ( hours === 0 ) {
                hours = "";
            } else {
                hours = hours.toString();
                hours += ":";
            }

            if ( minutes < 10 ) {
                minutes = "0" + minutes.toString();
            }

            if ( seconds < 10 ) {
                seconds = "0" + seconds.toString();
            }

            stamp = hours + minutes + ":" + seconds;


            return stamp;
        }
    };

    return Utils;
}());
module.exports = (function () {

    /**
    * Imports
    */
    var
        fs = require("fs"),
        http = require("http"),
        express = require("express"),
        request = require("request"),
        RenderConfig = require("./RenderConfig");

    /**
    * Constructor
    */
    function ShowFeed( req, resp ) {
        this.NS = "ShowFeed";
        this.url = req.query.url;
        this.resp = resp;

        // TODO: Make this not so overkill?
        this.renderConfig = new RenderConfig();
        this.get();
    }

    /**
    * Instance methods and properties
    */
    ShowFeed.prototype.options = {
        method: "GET",
        uri: "/api/feed?url="
    };

    ShowFeed.prototype.get = function () {
        var self = this;

        // Add requested URL to path
        self.options.uri += self.url;

        // Request Object
        // self.request = http.request( self.options, function ( resp ) {

        //     // Init Request binds
        //     self.initReqBinds.call( self, resp );
        // });

        self.request = request( self.options, function (error, response, body) {
            // console.log(body);
            var returnedBody = JSON.parse( body );
            // console.log(returnedBody.channel[0].item[0].description[0]);

            if ( returnedBody ) {
                console.log("success");
                self.render.call( self, returnedBody );
            } else {
                console.log("fail");
            }
        });
        // self.request = request.get( self.options.uri ).on("error", function () {
        //     console.log("error!");
        // }).on("response", function () {
        //     console.log("response!");
        // }).pipe(fs.createWriteStream('temp.json'));


        // Init Response Binds
        // self.initRespBinds();
    };

    ShowFeed.prototype.initReqBinds = function ( resp ) {
        var self = this,

            // Build the "body"
            body = "";

        // On 'data'...
        resp.on('data', function ( data ) {

            // ...add to "body"
            body += data;
        });

        // On 'end'
        resp.on('end', function () {

            // Turn "body" into JSON
            body = JSON.parse( body );

            // Render Body
            self.render.call( self, body );
        });
    };

    ShowFeed.prototype.initRespBinds = function () {

        // On Request Error...
        this.request.on('error', function ( e ) {

            // Do stuff
            console.log('Problem with request: ' + e.message);
        });

        // End Request
        this.request.end();
    };

    ShowFeed.prototype.render = function ( body ) {

        // Grab render options from feed body,
        // update render options object
        this.updateOptions( body );

        // Render results template
        this.resp.render( this.renderConfig.feed.tmpl, this.renderConfig.feed.options );
    };

    ShowFeed.prototype.updateOptions = function ( body ) {
        this.renderConfig.feed.options.title += " for " + body.channel[0].title[0];
        this.renderConfig.feed.options.showTitle = body.channel[0].title[0];
        this.renderConfig.feed.options.image = body.channel[0]["itunes:image"][0].$.href;
        this.renderConfig.feed.options.episodes = body.channel[0].item;
    };

    /**
    * Expose
    */
    return ShowFeed;
}());

module.exports = (function () {

    /**
    * Imports
    */
    var
        request = require("request");

    /**
    * Constructor
    */
    function Accounts () {
        this.NS = "Accounts";
    }

    /**
    * Instance methods and properties
    */
    
    Accounts.prototype.create = function ( req, resp, success, fail ) {
        var self = this,
            options = {
                uri: "http://localhost:1337/api/user?username=" + req.body.username + "&password=" + req.body.password,
                method: "POST"
            };

        // Request Object
        request( options, function (error, response, body) {
            var returnedBody = JSON.parse( body );

            if ( returnedBody ) {
                req.login( returnedBody, {}, function (err) {
                    if (err) {
                        console.log(err);
                    }
                    success();
                });
            } else {
                fail();
            }
        });
    };

    /**
    * Expose
    */
    return Accounts;
}());

module.exports = (function () {

    /**
    * Imports
    */
    var
        Events = require("events").EventEmitter,
        cheerio = require("cheerio"),
        Utils = require("./Utils")
        SoundCloud = require("./SoundCloud");

    /**
    * Constructor
    */
    var GetFeed = function GetFeed( body, url ) {
            this.verbose = true;
            this.NS = "GetFeed";
            // this.events = events;
            // this.resp = resp;
            this.body = body;
            this.url = url;
            this.$ = cheerio.load( this.body );
            this.feeds = [];
            this.soundCloud = false;

            return this.init();
        };

    /**
    * Instance methods and properties
    */
    GetFeed.prototype = {
        "defaults": {
            "page": {
                "headers": {
                    "User-Agent": "node.js"
                }
            },
            "feed": {
                "method": "GET",
                "url": "/api/feed?url="
            },
            "feedStrings": {
                "end": [
                    "/feed",
                    "/feed/",
                    "/feeds",
                    "/feeds/",
                    "/rss",
                    "/rss/",
                    "/podcast",
                    "/podcast/",
                    "feed.xsl",
                    "rss.xsl"
                ],
                "contain": [
                    "http://feeds.",
                    "https://feeds.",
                    "http://feed.",
                    "https://feed."
                ]
            }
        },
        "init": function init() {
            this.feeds = this.findFeedUrls();

            Utils.log(this.feeds);
            return this.feeds;
        },
        "isSoundcloud": function isSoundcloud() {
            return this.soundCloud;
        },
        "findFeedUrls": function findFeedUrls() {
            this.feeds = this.getLinkTag();
            this.feeds = this.getFeedLink();

            return this.feeds;
        },
        "getLinkTag": function getLinkTag() {
            var $rss = this.$("link[type='application/rss+xml']");

            if ( $rss.length ) {
                Utils.log("Link Tag Detected");

                $rss.each(function eachAudioTag( i, elem ) {
                    var $elem = this.$(elem),
                        file = Utils.makeFullPath( $elem.attr("href"), this.url );

                    if ( file && this.feeds.indexOf( file ) === -1 ) {
                        this.feeds.push( file );
                    }
                }.bind(this));
            }
            return this.feeds;
        },
        "getFeedLink": function getFeedLink() {
            var $rss = this.$( this.buildFeedLinkSelectors() );

            if ( $rss.length ) {
                Utils.log("Feed Link Detected");

                $rss.each(function eachAudioTag( i, elem ) {
                    var $elem = this.$(elem),
                        file = Utils.makeFullPath( $elem.attr("href"), this.url );

                    if ( this.feeds.indexOf( file ) === -1 ) {
                        this.feeds.push( file );
                    }
                }.bind(this));
            }
            return this.feeds;
        },
        "buildFeedLinkSelectors": function buildFeedLinkSelectors() {
            var selector = "";

            selector += this.buildFeedLinkSelectorsEnd();
            selector += this.buildFeedLinkSelectorsContain();

            return selector;
        },
        "buildFeedLinkSelectorsEnd": function buildFeedLinkSelectorsEnd() {
            var selector = "";

            this.defaults.feedStrings.end.forEach(function eachString(v, i) {
                if ( i > 0 ) {
                    selector += ", ";
                }
                selector += "a[href$='";
                selector += v;
                selector += "']";
            });

            return selector;
        },
        "buildFeedLinkSelectorsContain": function buildFeedLinkSelectorsContain() {
            var selector = "";

            this.defaults.feedStrings.contain.forEach(function eachString(v) {
                selector += ", ";
                selector += "a[href*='";
                selector += v;
                selector += "']";
            });

            return selector;
        }
    };

    /**
    * Expose
    */
    return GetFeed;
}());

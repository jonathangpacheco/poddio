module.exports = (function () {
    var settings = {
        "domain": "qast.fm",
        "ports": {
            "http": 80,
            "https": 443
        }
    };

    return settings;
}());
